package com.example.compressortest.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Customer {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    private byte[] binaryData;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private MyRandomData randomJsonData;

    protected Customer() {}

    public Customer(String firstName, String lastName, byte[] binaryData) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.binaryData = binaryData;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d, firstName='%s', lastName='%s']",
                id, firstName, lastName);
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


    public byte[] getBinaryData() {
        return binaryData;
    }

    public MyRandomData getRandomJsonData() {
        return randomJsonData;
    }

    public void setRandomJsonData(MyRandomData randomJsonData) {
        this.randomJsonData = randomJsonData;
    }
}