package com.example.compressortest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompressortestApplication {

    public static void main(String[] args) {
        SpringApplication.run(CompressortestApplication.class, args);
    }

}
