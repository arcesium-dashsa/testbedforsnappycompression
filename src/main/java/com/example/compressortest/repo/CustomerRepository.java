package com.example.compressortest.repo;

import com.example.compressortest.entity.Customer;
import com.example.compressortest.entity.MyRandomData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName);

    Customer findById(long id);
    @Query(
            value = "SELECT u.binary_data FROM customer u limit 50000",
            nativeQuery = true)
    List<byte[]> findAllBinaryData();

    @Query(
            value = "SELECT u.random_json_data FROM customer u limit 100000",
            nativeQuery = true)
    void findAllJsonBData();
}
