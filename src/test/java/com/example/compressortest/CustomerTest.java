package com.example.compressortest;

import com.example.compressortest.entity.Customer;
import com.example.compressortest.entity.MyRandomData;
import com.example.compressortest.repo.CustomerRepository;
import com.google.gson.Gson;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.xerial.snappy.Snappy;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@SpringBootTest
public class CustomerTest {
    Logger logger = LoggerFactory.getLogger(CustomerTest.class) ;
    @Autowired
    private CustomerRepository repo;

    @Test
     void loadCustomer(){

        EasyRandom generator = new EasyRandom();
        logger.info("Loading starts");
       for(int i =0 ; i <= 200000 ; i++) {
           try {
               generateAndLoadData(generator);
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       logger.info("Loading ends");

        logger.info("Query starts for binary data");
        repo.findAllBinaryData();
        logger.info("Query ends for binary data");

        /*logger.info("Query starts for jsonb data");
        repo.findAllJsonBData();
        logger.info("Query ends for jsonb data");*/

    }

    private void generateAndLoadData( EasyRandom generator) throws IOException {
        MyRandomData randomData = generator.nextObject(MyRandomData.class);
        String randomDataAsString =  new Gson().toJson(randomData);
        var bytes =  Snappy.compress(randomDataAsString.getBytes(StandardCharsets.UTF_8));
        Customer customer = new Customer("Satya","Dash", bytes);
        customer.setRandomJsonData(randomData);
        repo.save(customer);
    }

}
